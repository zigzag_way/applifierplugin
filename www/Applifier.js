var Applifier = {
    displayIncentivizedAdvert: function(successCallback, failureCallback, name, showCloseButton) {
        return cordova.exec(successCallback, failureCallback, 'ApplifierPlugin', 'displayIncentivizedAdvert', [name, showCloseButton]);
    },
    setUserID: function(successCallback, failureCallback, userID) {
        return cordova.exec(successCallback, failureCallback, 'ApplifierPlugin', 'setUserID', [userID]);
    },
    isVideoAvailable: function(successCallback, failureCallback) {
        return cordova.exec(successCallback, failureCallback, 'ApplifierPlugin', 'isVideoAvailable', []);
    }
}
module.exports = Applifier;
