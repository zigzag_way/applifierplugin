package com.phonegap.plugins;

import com.applifier.impact.android.ApplifierImpact;
import com.applifier.impact.android.IApplifierImpactListener;
import com.applifier.impact.android.properties.ApplifierImpactConstants;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class ApplifierPlugin extends CordovaPlugin implements IApplifierImpactListener {
	public static boolean fVideoAvailable = false;
	private static String userID = "";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callback) throws JSONException {
    	Log.v("ApplifierPlugin", "ApplifierPlugin execute method called (action = " + action + ")");

        try {
        	if (action.equals("displayIncentivizedAdvert")) {
        		// Open with options test
        		Map<String, Object> optionsMap = new HashMap<String, Object>();
        		optionsMap.put(ApplifierImpact.APPLIFIER_IMPACT_OPTION_NOOFFERSCREEN_KEY, true);
        		optionsMap.put(ApplifierImpact.APPLIFIER_IMPACT_OPTION_OPENANIMATED_KEY, false);
        		optionsMap.put(ApplifierImpact.APPLIFIER_IMPACT_OPTION_GAMERSID_KEY, userID);
        		optionsMap.put(ApplifierImpact.APPLIFIER_IMPACT_OPTION_MUTE_VIDEO_SOUNDS, false);
        		optionsMap.put(ApplifierImpact.APPLIFIER_IMPACT_OPTION_VIDEO_USES_DEVICE_ORIENTATION, false);
        		
        		ApplifierImpact.instance.showImpact(optionsMap);
        		// Open without options (defaults)
        		//ApplifierImpact.instance.showImpact();
            	
                callback.success();
            } else if(action.equals("isVideoAvailable")) {                
            	callback.success(ApplifierPlugin.fVideoAvailable ? "true" : "false");
            } else if(action.equals("setUserID")) {
            	userID = args.getString(0);
            	callback.success("setUserID = " + userID);
            } else {
            	if (true) {
	                Log.d("ApplifierPlugin", "invalid Applifier method: " + action);
	                callback.error("invalid Applifier method: " + action);
            	} else {
                    Log.d("ApplifierPlugin", "video not available");
                    callback.error("video not available");
            	}
                return false;
            }
            callback.success();
            return true; 
        } catch (Exception e){
            Log.d("ApplifierPlugin exception: ", e.getMessage());
            callback.error("ApplifierPlugin json exception: " + e.getMessage());
            return false;
        }
    }

    @Override
	public void onImpactClose () {
    	
    }
    
    @Override
	public void onImpactOpen () {
    	
    }
	
	// Impact video events
    @Override
	public void onVideoStarted () {
    	
    }
    
    @Override
	public void onVideoCompleted (String rewardItemKey, boolean skipped) {
    	if(skipped) {
    		Log.d(ApplifierImpactConstants.LOG_NAME, "Video was skipped!");
    	}
    }
	
	// Impact campaign events
    @Override
	public void onCampaignsAvailable () {
    	Log.d(ApplifierImpactConstants.LOG_NAME, "onCampaignsAvailable()");
    	fVideoAvailable = true;
	}
    
    @Override
    public void onCampaignsFetchFailed () {
    	fVideoAvailable = false;
    }
}
